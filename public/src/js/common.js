$(function(){
  setTimeout(function(){
    $('.wrapper').animate({'opacity':'1'});
    // $('.js-main-visual-content').addClass('is-view');
  },50);
});

$(window).on("load resize", function () {
  if (navigator.userAgent.match(/Android|Mobile|iP(hone|od|ad)|BlackBerry|IEMobile|Kindle|NetFront|Silk-Accelerated|(hpw|web)OS|Fennec|Minimo|Opera M(obi|ini)|Blazer|Dolfin|Dolphin|Skyfire|Zune/)) {
    $('body').addClass('device-mobile')
  } else {
    $('body').removeClass('device-mobile')
  }
  if ($(document).width() <= 1024) {
    $('body').addClass('media-mobile');
  } else {
    $('body').removeClass('media-mobile');
  }
}).resize();

// GNB
$(function () {
  var gnbItem = '.site-gnb__item';
  var gnbLink = '.site-gnb__link';

  /*$(gnbItem).on('mouseenter', function () {
    if ($('body').hasClass('media-mobile') || $('body').hasClass('device-mobile')) return;
    $(this).addClass('is-over');
  });
  $(gnbItem).on('mouseleave', function () {
    if ($('body').hasClass('media-mobile') || $('body').hasClass('device-mobile')) return;
    $(this).removeClass('is-over')
  });
  $(gnbItem).find('a').on('focusin', function () {
    if ($('body').hasClass('media-mobile') || $('body').hasClass('device-mobile')) return;
    $(this).closest(gnbItem).trigger('mouseenter');
  });
  $(gnbItem).find('a:last').on('focusout', function () {
    if ($('body').hasClass('media-mobile') || $('body').hasClass('device-mobile')) return;
    $(this).closest(gnbItem).trigger('mouseleave');
  });*/
  $(gnbLink).on('click', function(e) {
    if ($('body').hasClass('media-mobile') || $('body').hasClass('device-mobile')){
      if ($(this).closest(gnbItem).find('ul').length == 0) return;
      e.preventDefault();
      console.log(!$('body').hasClass('media-mobile') || !$('body').hasClass('device-mobile'));
      if ($(this).closest(gnbItem).hasClass('is-over')) {
        $(this).closest(gnbItem).removeClass('is-over');
      } else {
        $(this).closest(gnbItem).addClass('is-over');
      }
    }
  });
  $(window).resize(function () {
    if (!$('body').hasClass('media-mobile')) $(gnbItem).removeClass('is-over');
    gnbActive('.site-gnb__item.active');
  });

    function gnbActive(target) {
      if($(gnbItem).length > 0){
        setTimeout(function() {
          var activeItemSize = $(target).width() - 60;
          var offsetLeft = $(target).offset().left + 30;
          var activeBar = '.site-gnb__active-bar';
          $(activeBar).css({
            "width": activeItemSize,
            "left": offsetLeft
          });
        },51);
      }
    }
  $(gnbItem).on('mouseenter', function () {
    if ($('body').hasClass('media-mobile') || $('body').hasClass('device-mobile')) return;
    gnbActive(this);
  });
  $(gnbItem).on('mouseleave', function () {
    if ($('body').hasClass('media-mobile') || $('body').hasClass('device-mobile')) return;
    gnbActive('.site-gnb__item.active');
  });
  $(gnbItem).find('a').on('focusin', function () {
    if ($('body').hasClass('media-mobile') || $('body').hasClass('device-mobile')) return;
    $(this).closest(gnbItem).trigger('mouseenter');
  });
  $(gnbItem).find('a:last').on('focusout', function () {
    if ($('body').hasClass('media-mobile') || $('body').hasClass('device-mobile')) return;
    $(this).closest(gnbItem).trigger('mouseleave');
  });
});

// header fixed
// $(function() {
//   if ($('.site-header').length > 0) {
//     $(window).on("scroll resize", function (e) {
//       if (!$('body').hasClass('media-mobile')) {
//         headerFixed('.site-header');
//         $('.mobile-header').removeClass('is-fixed');
//       } else {
//         headerFixed('.mobile-header');
//         $('.site-header').removeClass('is-fixed');
//       }
//     });
//     function headerFixed(target) {
//       console.log($(target)[0].offsetTop);
//       if (!$(target).attr('data-top')) $(target).attr('data-top', $(target)[0].offsetTop);
//       if ($(this).scrollTop() > $(target).attr('data-top')) {
//         $(target).addClass('is-fixed');
//       } else {
//         $(target).removeClass('is-fixed');
//       }
//     }
//   }
// });

// mobile menu
$(function () {
  if ($('.mobile-menu').length > 0) {
    $(document).on('click', '.mobile-button-open, .mobile-button-close', function () {
      if ($('.mobile-menu').hasClass('is-on')) {
        $('.mobile-menu').removeClass('is-on')
        // $('body').removeClass('is-not-scroll')
        $('.mobile-menu-dimmed').remove();
      } else {
        $('.mobile-menu').addClass('is-on')
        // $('body').addClass('is-not-scroll')
        $('<div class="mobile-menu-dimmed"></div>').appendTo($(document.body))
      }
    })
    $(window).resize(function () {
      if (!$('body').hasClass('media-mobile')) {
        $('.mobile-menu').removeClass('is-on');
        // $('body').removeClass('is-not-scroll');
        $('.mobile-menu-dimmed').remove();
        $('.mobile-button-open').attr('disabled', 'disabled');
        $('.mobile-button-close').attr('disabled', 'disabled');
      } else {
        $('.mobile-button-open').removeAttr('disabled');
        $('.mobile-button-close').removeAttr('disabled');
      }
    }).trigger("resize");
  }
});

// 아코디언 목록
$(function(){
  if ($('.js-accordion-button').length > 0) $('.js-accordion-button').attr("tabindex","0");
  $('.js-accordion-button').on("click keypress",function(e){
    if ((e.keyCode == 13)||(e.type == 'click')) {
      $(this).next('.js-accordion-body').slideToggle().end().toggleClass('is-active');
    }
  });
});