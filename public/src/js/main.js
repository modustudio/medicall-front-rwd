/*$(function() {
  var wrapperMinHeight = 740;
  var windowHeight = 0;
  var topBannerHeight = 0;
  var footerHeight = 0;
  $(window).on('resize', function (e) {
    windowHeight = $(window).height();
    footerHeight = $('.site-footer').outerHeight();
    //띠배너 있으면
    if($('.top-banner').length > 0){
      topBannerHeight = $('.top-banner').outerHeight();
    }
    if ($('body').hasClass('media-mobile') || $('body').hasClass('device-mobile')){
      $('.site-container').height($(window).height());
    } else {
      if($('.wrapper').height() > wrapperMinHeight){
        $('.site-container').height(windowHeight - topBannerHeight - footerHeight);
      } else {
        $('.site-container').height(wrapperMinHeight - footerHeight);
      }
    }
  }).trigger('resize');
});*/

// 띠배너
$(function () {
  if($('.top-banner').length > 0){
    var topBanner = '.top-banner';
    setTimeout(function(){
      $(topBanner).addClass('is-show');
    },500);
  }

  $(topBanner).find('.top-banner__close').on('click', function () {
    if($(topBanner).hasClass('is-show')){
      $(topBanner).removeClass('is-show');
    }
  });
});

$(function () {
  var quickItem = '.main-quick-menu__item';
  $(quickItem).on('mouseenter', function () {
    if ($('body').hasClass('media-mobile') || $('body').hasClass('device-mobile')) return;
    $(this).addClass('is-over');
  });
  $(quickItem).on('mouseleave', function () {
    if ($('body').hasClass('media-mobile') || $('body').hasClass('device-mobile')) return;
    $(this).removeClass('is-over')
  });
  $(quickItem).find('a').on('focusin', function () {
    if ($('body').hasClass('media-mobile') || $('body').hasClass('device-mobile')) return;
    $(this).closest(quickItem).trigger('mouseenter');
  });
  $(quickItem).find('a:last').on('focusout', function () {
    if ($('body').hasClass('media-mobile') || $('body').hasClass('device-mobile')) return;
    $(this).closest(quickItem).trigger('mouseleave');
  });
});