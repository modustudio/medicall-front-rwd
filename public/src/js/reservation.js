//달력
$(document).ready(function() {
  var calendar =  $('#calendar').fullCalendar({
    locale: 'en',
    height: 'auto',
    header: {
      center: 'prev title next',
      left: '',
      right: ''
    },
    buttonText:{
      prev: '<',
      next: '>'
    },
    editable: false,
    firstDay: 0,
    selectable: true,
    defaultView: 'month',
    titleFormat:'YYYY.MM',
    axisFormat: 'h:mm',
    allDaySlot: true,
    dayRender: function (date, cell) {

    },
    dayClick: function (date, jsEvent, view) {
      layerPopup("#layer-popup-a");
    }
  });
});
function layerPopup(el) {
  var isDim = $("body").attr("has-dimmed");
  if ($(el).length === 0) {
    alert("레이어팝업 없음");
    return false;
  }
  if(!isDim){
    $("body").attr("has-dimmed",true);
    $('body').addClass('is-not-scroll');
    $(".dimmed").fadeIn();
    $(el).fadeIn();
  }
}
function closeLayerPopup(el) {
  var target = $(el).attr('href');
  $("body").removeAttr("has-dimmed");
  $('body').removeClass('is-not-scroll');
  $(".dimmed").fadeOut();
  $(target).fadeOut();
}